//
//  AuthModel.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

struct AuthModel: Codable {
    
    var sub: String
    var token: String
    var type: String
    
}
