//
//  ErrorModel.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

struct ErrorModel: Codable {
    var code: String
    var message: String    
}
