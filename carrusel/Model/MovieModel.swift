//
//  MovieModel.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

struct MovieModel: Codable {
    var title: String
    var type: String
    var items: [ItemModel]
}

struct ItemModel: Codable {
    var title: String
    var imageUrl: String?
    var videoUrl: String?
    var description: String
}
