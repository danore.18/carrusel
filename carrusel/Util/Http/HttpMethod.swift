//
//  HttpMethod.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

enum HttpMethod: String {
    
    case post
    case put
    case get
    case delete
    
    var value: String? {
        switch self {
        case .post:
            return "POST"
        case .put:
            return "PUT"
        case .get:
            return "GET"
        case .delete:
            return "DELETE"
        }
    }
}
