//
//  Routes.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

let API_BASE = "https://echo-serv.tbxnet.com"

let AUTH_URL = "\(API_BASE)/v1/mobile/auth"
let MOVIES_URL = "\(API_BASE)/v1/mobile/data"
