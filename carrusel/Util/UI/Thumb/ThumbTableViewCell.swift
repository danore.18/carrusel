//
//  ThumbTableViewCell.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation
import UIKit

class ThumbTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionThumbView: UICollectionView!
}

extension ThumbTableViewCell {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {

        collectionThumbView.delegate = dataSourceDelegate
        collectionThumbView.dataSource = dataSourceDelegate
        collectionThumbView.register(UINib(nibName: "\(ItemThumbCollectionViewCell.self)", bundle: nil), forCellWithReuseIdentifier: "cellThumb")
        collectionThumbView.tag = row
        collectionThumbView.setContentOffset(collectionThumbView.contentOffset, animated:false)
        collectionThumbView.reloadData()
    }

    var collectionViewOffset: CGFloat {
        set { collectionThumbView.contentOffset.x = newValue }
        get { return collectionThumbView.contentOffset.x }
    }
}
