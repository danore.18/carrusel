//
//  Extension+Helper.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

extension URLComponents {
    
    mutating func setQueryItems(with parameters: [String: Any]) {
        self.queryItems = parameters.map { URLQueryItem(name: $0.key, value: $0.value as? String) }
    }
    
}

extension Notification.Name {
    static let didSessionExpired = Notification.Name("didSessionExpired")
}
