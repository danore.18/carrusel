//
//  Preferences+Helper.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

class Preferences {
    
    static var shared = Preferences()
    private let defaults: UserDefaults
    
    private init() {
        self.defaults = UserDefaults.standard
    }
    
    var validSession: Bool {
        return defaults.bool(forKey: EXIST_SESSION_KEY)
    }
    
    var logout: Void {
        defaults.set(false, forKey: EXIST_SESSION_KEY)
        defaults.set(nil, forKey: AUTHORIZATION_KEY)
    }
    
    var token: String? {
        return defaults.string(forKey: AUTHORIZATION_KEY)
    }
    
    func saveSession(_ param: String) {
        defaults.set(param, forKey: AUTHORIZATION_KEY)
        defaults.set(true, forKey: EXIST_SESSION_KEY)
    }
}
