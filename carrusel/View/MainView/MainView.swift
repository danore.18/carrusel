//
//  MainView.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation
import UIKit

class MainView: UIViewController {
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(onDidSessionExpired(_:)), name: .didSessionExpired, object: nil)
    }
    
    func showError(_ text: String) {
        let alert = UIAlertController(title: "Error", message: text, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func onDidSessionExpired(_ notification: Notification)
    {
//        Preferences.shared.logout
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: LOGIN_VIEW)
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destViewController = segue.destination as! UINavigationController
        
        if (segue.identifier!.elementsEqual(HOME_VIEW)) {
            if let _ : HomeView = destViewController.topViewController as? HomeView {
            }
        }
    }
    
}
