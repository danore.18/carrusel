//
//  HomeView.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation
import UIKit
import AVKit

class HomeView: MainView, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let moviewVM = MovieViewModel()
    var storedOffsets = [Int: CGFloat]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialize()
    }
    
    private func initialize() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.moviewVM.getData {
            self.tableView.reloadData()
        } onError: { error in
            self.showError(error)
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.moviewVM.items.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let section = self.moviewVM.items[section]
        return section.title
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = self.moviewVM.items[indexPath.section]
        
        if section.type == "thumb" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "thumb", for: indexPath) as! ThumbTableViewCell
            cell.collectionThumbView.delegate = self
            cell.collectionThumbView.dataSource = self
            cell.collectionThumbView.tag = indexPath.section
            cell.collectionThumbView.reloadData()
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "poster", for: indexPath) as! PosterTableViewCell
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.tag = indexPath.section
            cell.collectionView.reloadData()
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section = self.moviewVM.items[indexPath.section]
        return section.type == "thumb" ? 150 : 300
    }
    
}

extension HomeView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let section = self.moviewVM.items[collectionView.tag]
        return section.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let section = self.moviewVM.items[collectionView.tag]
        let entity = section.items[indexPath.row]
        
        if section.type == "thumb" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellThumb", for: indexPath) as! ItemThumbCollectionViewCell
            
            cell.lblTitle.text = entity.title
            
            ImageLoader.image(for: URL(string: entity.imageUrl ?? "http://placehold.it/200x300")!) { image in
                cell.imgCover.image = image
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellPoster", for: indexPath) as! ItemPosterCollectionViewCell
            
            cell.lblTitle.text = entity.title
            
            ImageLoader.image(for: URL(string: entity.imageUrl ?? "http://placehold.it/200x300")!) { image in
                cell.imgCover.image = image
            }
            
            return cell
        }
    }
    
    func collectionView(_ colectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let section = self.moviewVM.items[indexPath.section]
        let entity = section.items[indexPath.row]
        
        entity.videoUrl != nil
            ? self.playVideo(entity.videoUrl!)
            : self.showError("Video no disponible")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let section = self.moviewVM.items[collectionView.tag]
        return section.type == "thumb" ? CGSize(width: 250, height: 150) : CGSize(width: 150, height: 300)
    }
    
    private func playVideo(_ url: String) {
        guard let videoURL = URL(string: url) else {
            return
        }
        
        let asset = AVAsset(url: videoURL)
        let playerItem = AVPlayerItem(asset: asset)
        let player = AVPlayer(playerItem: playerItem)
        
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        playerViewController.player?.volume = 25
        
        self.present(playerViewController, animated: true) {
            playerViewController.player?.play()
        }
    }
}
