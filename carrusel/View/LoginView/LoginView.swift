//
//  LoginView.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation
import UIKit

class LoginView: MainView {
    
    @IBOutlet weak var btnLogin: UIButton!
    
    var authVM = AuthViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialize()
    }
    
    @IBAction func auth(_ sender: Any) {
        self.authVM.auth { response in
            self.performSegue(withIdentifier: HOME_VIEW, sender: self)
        } onError: { error in
            self.showError(error)
        }

    }
    
    private func initialize() {
        self.btnLogin.layer.cornerRadius = 12
    }
    
}
