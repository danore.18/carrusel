//
//  MovieViewModel.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

class MovieViewModel {
    
    let network: NetworkManager
    
    var items = [MovieModel]()
    
    init() {
        self.network = NetworkManager()
    }
    
    func getData(_ onSuccess: @escaping () ->(), onError: @escaping (String) ->()) {
        self.network.request(MOVIES_URL, method: .get) { data, success in
            guard let info = data else {
                onError("Results not found")
                return
            }
            
            do {
                if success {
                    self.items = try JSONDecoder().decode([MovieModel].self, from: info)
                    onSuccess()
                } else {
                    let error = try JSONDecoder().decode(ErrorModel.self, from: info)
                    onError(error.message)
                }
            }  catch {
                onError("Parse error: \(error)")
            }
        }
    }
    
}
