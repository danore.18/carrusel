//
//  AuthViewModel.swift
//  carrusel
//
//  Created by Daniel Orellana on 3/07/21.
//

import Foundation

class AuthViewModel {
    
    let network: NetworkManager
    
    init() {
        self.network = NetworkManager()
    }
    
    func auth(_ onSuccess: @escaping (AuthModel) ->(), onError: @escaping (String) ->()) {
        self.network.request(AUTH_URL, params: ["sub": BODY_AUTH_PARAM],  method: .post) { data, success in
            guard let info = data else {
                onError("Results not found")
                return
            }
            
            do {
                if success {
                    let result = try JSONDecoder().decode(AuthModel.self, from: info)
                    self.commitSession(result)
                    
                    onSuccess(result)
                } else {
                    let error = try JSONDecoder().decode(ErrorModel.self, from: info)
                    onError(error.message)
                }
            }catch {
                onError("Parse error")
            }
        }
    }
    
    private func commitSession(_ data: AuthModel) {
        let authorizationToken = "\(data.type) \(data.token)"
        Preferences.shared.saveSession(authorizationToken)
    }
    
}
